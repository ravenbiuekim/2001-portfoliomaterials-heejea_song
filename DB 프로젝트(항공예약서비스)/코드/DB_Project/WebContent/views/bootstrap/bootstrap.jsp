﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Link+ DataBase Applications</title>
<meta charset="UTF-8">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/codingBooster.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/lightgallery.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />

<!-- 웹 폰트 -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
	rel="stylesheet">

	
</head>
<body>
	
					${customer} 
				<a>${customer.customerid}</a>
				 <a>${customer.customername}</a>

			<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<h1>Project Name: Airline reservation system <span><br />
					</span></h1>
					
				</div>
			</section>
			
	<div id="wrap">
		<div id="header" role="header">
			<div class="container">
				<div class="header">
					<!-- //헤더 메뉴 -->
					<div class="inner" >
					<a href="${pageContext.request.contextPath}/customer/main.do" class="logo">DatabaseMasterTeam</a>

					<nav id="nav">
						<c:choose>
						<c:when test="${empty customerid}">
						<a href="${pageContext.request.contextPath}/customer/login.do">로그인</a>
						<a href="${pageContext.request.contextPath}/customer/join.do">회원가입</a>
						</c:when>
						<c:otherwise>
						<a href="" >${customerid}님 환영합니다</a>
						<a href="${pageContext.request.contextPath}/customer/logout.do">로그아웃</a>
						<a href="${pageContext.request.contextPath}/reservation/mypage.do?userid=${customerid}">MyReservation</a>
						<a href = "${pageContext.request.contextPath}/customer/mypagelist.do?userid=${mylist.userid}">Mypage</a>
						</c:otherwise>
						</c:choose>
					</nav>
					</ul>
					
				</nav>
				</div>
				</div>
			</div>
		</div>
	</div>


	</body>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/lightgallery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/lightgallery-all.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
</body>

</html>