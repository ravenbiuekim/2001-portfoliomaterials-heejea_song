<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html>
<html lang="ko">
<head>
<title>나의 예약리스트</title>
 <%@ include file="/views/common.jsp"%>
</head>
<body>
<div style="padding:100px">
<h2>나의 예약관리</h2>
<hr>
<br>
<div align="center">
<form action="${pageContext.request.contextPath}/customer/join.do" method="post">
		<table class="table">
			<tr>
				<th>예매번호:</th>
				<td>${mylist.reservationno }</td>
			</tr>
			<tr>
				<th>항공사 :</th>
				<td>${mylist.airlinename }</td>
			</tr>
			<tr>
				<th>출발지 :</th>
				<td>${mylist.startpoint }</td>
			</tr>
			<tr>
				<th>도착지 :</th>
				<td>${mylist.finishpoint }</td>
			</tr>
			<tr>
				<th>탑승시간 :</th>
				<td>${mylist.boardingtime }</td>
			</tr>
			<tr>
				<th>좌석등급 :</th>
				<td>${mylist.seatno }</td>
			</tr>
			<tr>
				<th>노선번호 :</th>
				<td>${mylist.routeno }</td>
			</tr>
			<tr>
				<th>회원ID :</th>
				<td>${mylist.userid }</td>
			</tr>			
			<tr>
				<th>비행기 번호 :</th>
				<td>${mylist.airlineno }</td>
			</tr>
		</table>
		<a href = "${pageContext.request.contextPath}/customer/redelete.do?reservationno=${mylist.reservationno }"><input class="btn btn-success" type="button" value="예약리스트 삭제"></a>
		<a href = "${pageContext.request.contextPath}/customer/myrelist.do?userid=${mylist.userid}"><input class="btn btn-success" type="button" value="나의 예약리스트 확인하기"></a>
				<a href = "${pageContext.request.contextPath}/customer/mypagelist.do?userid=${mylist.userid}"><input class="btn btn-success" type="button" value="Mypage"></a>
	</form>
</div>
</div>
</body>
</html>