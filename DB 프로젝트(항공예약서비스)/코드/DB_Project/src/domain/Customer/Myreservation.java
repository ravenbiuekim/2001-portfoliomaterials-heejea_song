package domain.Customer;

public class Myreservation {

	private String reservationno;
	private String boardingtime;
	private String startpoint;
	private String finishpoint;
	private String airlinename;
	private String airlineno;
	private String routeno;
	private String userid;
	private String seatno;
	public String getReservationno() {
		return reservationno;
	}
	public void setReservationno(String reservationno) {
		this.reservationno = reservationno;
	}
	
	public String getAirlineno() {
		return airlineno;
	}
	public void setAirlineno(String airlineno) {
		this.airlineno = airlineno;
	}
	public String getBoardingtime() {
		return boardingtime;
	}
	public void setBoardingtime(String boardingtime) {
		this.boardingtime = boardingtime;
	}
	public String getStartpoint() {
		return startpoint;
	}
	public void setStartpoint(String startpoint) {
		this.startpoint = startpoint;
	}
	public String getFinishpoint() {
		return finishpoint;
	}
	public void setFinishpoint(String finishpoint) {
		this.finishpoint = finishpoint;
	}
	public String getAirlinename() {
		return airlinename;
	}
	public void setAirlinename(String airlinename) {
		this.airlinename = airlinename;
	}
	public String getRouteno() {
		return routeno;
	}
	public void setRouteno(String routeno) {
		this.routeno = routeno;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getSeatno() {
		return seatno;
	}
	public void setSeatno(String seatno) {
		this.seatno = seatno;
	}
}
