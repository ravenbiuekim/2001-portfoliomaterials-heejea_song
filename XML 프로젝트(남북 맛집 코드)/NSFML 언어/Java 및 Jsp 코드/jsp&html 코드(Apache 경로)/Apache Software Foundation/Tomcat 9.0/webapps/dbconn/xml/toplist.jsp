<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr" import="java.sql.*"%>
<!--문서의 정보를 전달해주는 기능을 한다. contentTtype : 출력 형태를 지정, charset : 응답 결과 출력 인코딩 방식 지정, pageEncoding : 문서 시작 시 문서의 인코딩 방식 지정
import : 해당 문서 내에서 사용 할 기능울 정의한다.-->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--해당 문서의 DTD 연결한다.-->

<html>
	<head>
		<meta charset="EUC-KR">
		<title>Top List</title>
	</head>
	<body bgcolor="#EEEEEE">
	<h1 align="center"><b>이북식 식당</b></h1>
	<hr>
<% request.setCharacterEncoding("euc-kr"); %>
<!--해당 문서 내 한글 폰트 손상 관리한다.-->
<%
	Connection conn = null;
	PreparedStatement pstmt = null;	
	// DB와 연결된 커녁션을 생성 후 필요시에만 임시로 빌려서 사용 후 반환하는 방식이다.
	//DB 연결로부터 SQL을 실행이 가능하도록 하는 메소드이며 SQL문을 미리 생성 후 변수를 입력받는 방식이다.

	String jdbc_driver = "com.mysql.jdbc.Driver";
	String jdbc_url = "jdbc:mysql://localhost:3306/xml";
	 // 자바에서 DB로 접근하는 정보 변수이다.
	 // 해당 DB에 대한 경로이다.
	try{
		Class.forName(jdbc_driver);		
		// DB 드라이버를 확인한다.
		conn = DriverManager.getConnection(jdbc_url,"root","1234");
		// 해당 DB에 접근한다.
		String sql = "select * from nkrest order by 1 asc";
		// SQL구문을 작성한다. 1울 기준으로 오름차순 정렬하여 모든 결과값을 출력한다.
		pstmt = conn.prepareStatement(sql);
		// 연결한 DB에 해단 sql문을 입력한다.

		ResultSet rs = pstmt.executeQuery();
		 // sql문이 실행된 값이 rs 객체로 반환된다.
		int i=1;
		// i를 1로 초기화한다. 즉 i값이 증가되면서 순서대로 출력하기 때문이다.
		Boolean check = false;
		// check라는 부울변수를 생성하고 초기값은 false를 준다.


%>

<style>
<!--
td { font-size : 9pt; }
A:link { font : 9pt; color : black; text-decoration : none;
font-family: 굴림; font-size : 9pt; }
A:visited { text-decoration : none; color : black;
font-size : 9pt; }
A:hover { text-decoration : underline; color : black;
font-size : 9pt;}
-->
</style>
</head>
<table width=1500 border=0 cellpadding=2 cellspacing=1 bgcolor=#777777 align=center>
<tr>
		<th bgcolor="#999999" align=center>No</th>
		<th bgcolor="#999999" align=center>이북식당 이미지</th>
		<th bgcolor="#999999" align=center>이북식당 이름</th>
		<th bgcolor="#999999" align=center>이북식당 위치</th>
		<th bgcolor="#999999" align=center>이북식당 메뉴</th>
		<th bgcolor="#999999" align=center>이북식당 번호</th>
		<th bgcolor="#999999" align=center>이북식당 특징</th>
</tr>
<%	while(rs.next()) // rs.next()를 이용하여 값이 있을 때 까지 반복을 돌린다. 
	{
		check = true; // check가 참일 경우 해당 테이블을 생성한다.
			%>
			<tr>
				<td bgcolor="#EEEEEE"><%=i%></td>
				<td bgcolor="#EEEEEE" width=200><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/<%=rs.getString("North_Korea_img")%>" width="200" height="100"/></td>
				<td bgcolor="#EEEEEE" width=300 align=center><%=rs.getString("North_Korea_name")%></td>
				<td bgcolor="#EEEEEE" width=500 align=center><%=rs.getString("North_Korea_location")%></td>
				<td bgcolor="#EEEEEE" width=300 align=center><%=rs.getString("North_Korea_menu")%></td>
				<td bgcolor="#EEEEEE" width=300 align=center><%=rs.getString("North_Korea_number")%></td>
				<td bgcolor="#EEEEEE" width=800 align=center><%=rs.getString("North_Korea_Explantion")%></td>
			</tr>
<%		i++; //i를 증가시켜준다. 
		}
		rs.close();
		pstmt.close();
		conn.close();
		// sql 실행과 db 연결을 헤지한다. 
	}
	catch(Exception e) {
		System.out.println(e);
		// 에러 발생시 e를 출력한다.
	}
%>
	</table><!--경로 이동을 위한 테이블을 생성한다. -->
	<table border=1 align="center">
	<tr>
		<td>		
		<input type="button"  value="음식 목록 화면으로 가기" onClick="location.href='list.jsp'"></td>
	</tr>
	</table>

</body>
</html>
