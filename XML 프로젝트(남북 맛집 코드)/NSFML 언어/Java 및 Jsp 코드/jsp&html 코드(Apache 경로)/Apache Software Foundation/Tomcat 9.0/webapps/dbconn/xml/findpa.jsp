<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<!--문서의 정보를 전달해주는 기능을 한다. contentTtype : 출력 형태를 지정, charset : 응답 결과 출력 인코딩 방식 지정, pageEncoding : 문서 시작 시 문서의 인코딩 방식 지정
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
	<title>아이디 및 비밀번호 찾기 페이지</title> <!--아이디와 비밀번호를 찾기 위한 테이블을 생성한다.-->
    </head>
    <body bgcolor="#EAEAEA">
	<form action="findid.jsp" method="post"> 
	<h1 align="center">아이디 찾기 페이지 입니다.</h1>

	<table border="1" width="500" align="center">
		<tr>
			<td bgcolor="#E0FFDB">이름 :</td> 
            <td bgcolor= "#FFFFFF"><input width="180" name="fname" type="text"></td><br>
			<th bgcolor= "#FFFFFF" rowspan="3" align="center"> 
			<input type="submit" value="이름으로 아이디 찾기"/>
		</tr>
		<tr>
			<td bgcolor="#E0FFDB">전화번호 :</td> 
			<td bgcolor= "#FFFFFF"><input width="180" name="fphone" type="text"></td><br>
		</tr>
		<tr>
			<td bgcolor="#E0FFDB">이메일 :</td> 
			<td bgcolor= "#FFFFFF"><input width="180" name="femail" type="text"></td>
		</tr>
		<!--아이디 찾기를 위해 이름, 전화번호, 이메일에 대한 값을 입력한 후 이름으로 아이디 찾기 버튼을 누를 경우 findid로 넘어간다..-->
			</th>
		</tr>
		</table>
	</form>
	<hr>

	<form action="find.jsp" method="post">
	<h1 align="center">비밀번호 찾기 페이지 입니다.</h1>

	<table border="1" width="500" align="center">
		<tr>	
			<td bgcolor="#E0FFDB">아이디 :</td> 
	        <td bgcolor= "#FFFFFF"><input name="fid" type="text"></td>
			</th>
			<th bgcolor= "#FFFFFF">
			<input type="submit" value="아이디로 비밀번호 찾기">
			</th>
		</tr>
	</table>
		<table border="1" width="500" align="center">
		<tr>
			<th bgcolor= "#FFFFFF">
			<input type="button"  value="로그인 화면으로 가기" onClick="location.href='login.jsp'">			
			<input type="button"  value="회원가입하러 가기" onClick="location.href='newsignup.html'">
			</th>
		</tr>
		<!--비밀번호를 찾기 위해서 아이디를 입력하게 될경우 find.jsp로 넘어가게 되며, 로그인화면으로가기, 회원가입하러가기 버튼을 생성한다. ..-->
	</table>	
	<hr>
	</form>
    </body>	
</html>