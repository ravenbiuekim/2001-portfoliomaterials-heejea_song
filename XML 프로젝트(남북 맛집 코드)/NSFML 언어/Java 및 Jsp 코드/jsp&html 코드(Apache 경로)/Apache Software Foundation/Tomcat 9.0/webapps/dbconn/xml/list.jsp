<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<!--문서의 정보를 전달해주는 기능을 한다. contentTtype : 출력 형태를 지정, charset : 응답 결과 출력 인코딩 방식 지정, pageEncoding : 문서 시작 시 문서의 인코딩 방식 지정
한다.-->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--해당 문서의 DTD 연결한다.-->
<html>
 <title> 이북식 음식 레시피 확인하러가기 </title>
 <body bgcolor="#EEEEEE"></body>
<body topmargin=0 leftmargin=0 text=#464646>
<center>
<BR>

	<form method="post" align="center">
	<table border="0" width="500" align="center">
	<tr>
	<td bgcolor="#EEEEEE" width="150" align="center">
	<h1>
	<%
     out.print(session.getAttribute("id") + "님 환영합니다"); // 해당 아이디
    %>
  	</td>
	</table>

<hr>
<table width=580 border=0 cellpadding=2 cellspacing=1 bgcolor=#777777>
<tr>
    <td height=20 colspan=8 align=center bgcolor=#999999>
        <font color=white><B>북한 음식 선택
      </B></font>
    </td>
</tr>
<tr>
    <td width=50 height=20 align=center bgcolor=#EEEEEE>list-1</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/평양온반.JPG" onclick="location.href='recipe1.jsp'" width="240" height="190"/></a></td>
    <td width=50 height=20 align=center bgcolor=#EEEEEE>list-2</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/녹두지짐.JPG"
	onclick="location.href='recipe2.jsp'" width="240" height="190"/></a></td>
	 <td width=50 height=20 align=center bgcolor=#EEEEEE>list-3</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/언감자떡.JPG" onclick="location.href='recipe3.jsp'" width="240" height="190"/></a></td>
    <td width=50 height=20 align=center bgcolor=#EEEEEE>list-4</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/갓김치.JPG"  onclick="location.href='recipe4.jsp'"width="240" height="190"/></a></td>
</tr>
<tr>
    <td width=50 height=20 align=center bgcolor=#EEEEEE>list-5</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/전천닭튀기.JPG"  onclick="location.href='recipe5.jsp'"width="240" height="190"/></a></td>
    <td width=50 height=20 align=center bgcolor=#EEEEEE>list-6</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/더덕볶음.JPG"  onclick="location.href='recipe6.jsp'" width="240" height="190"/></a></td>
	 <td width=50 height=20 align=center bgcolor=#EEEEEE>list-7</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/해주비빔밥.JPG" onclick="location.href='recipe7.jsp'" width="240" height="190"/></a></td>
    <td width=50 height=20 align=center bgcolor=#EEEEEE>list-8</td>
    <td width=240 bgcolor=white><img src="C:/Program Files/Apache Software Foundation/Tomcat 9.0/webapps/dbconn/xml/file/바스레기두부탕.JPG"  onclick="location.href='recipe8.jsp'" width="240" height="190"/></a></td>
</tr>
</table>
<input type="button"  value="이북음식 Top 7 List 확인하러가기" onClick="location.href='toplist.jsp'">
<input type="button"  value="로그아웃" onClick="location.href='logout.jsp'">

</body>
</html>
<!--이북식 음식에 관련하여 리스트를 생성하였으며 해당 리스트를 클릭했을 경우에 음식을 좀더 세부적으로 알려주는 페이지로 넘어간다.-->