<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<!--문서의 정보를 전달해주는 기능을 한다. contentTtype : 출력 형태를 지정, charset : 응답 결과 출력 인코딩 방식 지정, pageEncoding : 문서 시작 시 문서의 인코딩 방식 지정
한다.-->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--해당 문서의 DTD 연결한다.-->
<html>
    <head>
        <meta charset="EUC-KR">
    </head>
	<title> 로그인중입니다. </title> <!--로그인중일때에 대한 테이블을 생성한다.-->
    <body bgcolor="#EAEAEA">
		<h2 align="center">로그인 완료</h2>
		<hr>
		<form method="post" align="center">
		<table border="1" width="500" align="center">
		<tr>
			<td bgcolor="#FFFFFF" width="150" align="center">
			<%
      out.print(session.getAttribute("id") + "님 환영합니다"); // 해당 아이디에 대한 값에 따라 환영한다고 출력해준다.
      String Master = (String)session.getAttribute("Master"); // request에서 master파라미터를 가져오고 그 값을 Master라는 변수에 입력해준다.
      if("Yes".equals(Master)) // 만약에 Master에 들어있는 값과 Yes를 비교해서 일치한다면 관리자 여부를 Yes로 나타낸다.(테이블을 이용하여)
      {
      %>
        <td bgcolor="#FFFFFF" width="150" align="center">
      <%
        out.print("관리자 여부 : Yes");
      %>
          <tr>
            <td bgcolor="#FFFFFF" colspan="2" align="center">
            <input type="button"  value="로그아웃" onClick="location.href='logout.jsp'">
            <input type="button"  value="회원 리스트 및 DB 확인" onClick="location.href='DB_con.jsp'">
            <input type="button"  value="이북식 레시피 확인하러 가기" onClick="location.href='list.jsp'">
           </td>
         </tr>
       </td>
      <%
      }
      else // 만약에 Master에 들어있는 값과 Yes를 비교해서 일치하지 않는다면 관리자 여부를 No로 나타낸다.(테이블을 이용하여)
      {  %>
        <%
 //         out.print("관리자 여부 : No");
		  out.println("<script>");
		  out.println("alert('로그인 되었으며 이북식 음식 리스트로 갑니다.')");
	      out.println("location.href='list.jsp'");
		  out.println("</script>");
		%>
        <%
       }
         %>
      </td>
		</table>
	  </body>
</html>
