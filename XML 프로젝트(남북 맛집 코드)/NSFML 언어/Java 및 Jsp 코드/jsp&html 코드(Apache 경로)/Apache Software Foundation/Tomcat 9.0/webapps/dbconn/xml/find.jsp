<%@ page language="java" contentType="text/html; charset=euc-kr" import="java.sql.*" pageEncoding="euc-kr" %>
<!--문서의 정보를 전달해주는 기능을 한다. contentTtype : 출력 형태를 지정, charset : 응답 결과 출력 인코딩 방식 지정, pageEncoding : 문서 시작 시 문서의 인코딩 방식 지정
import : 해당 문서 내에서 사용 할 기능울 정의한다.-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--해당 문서의 DTD 연결한다.-->
<% request.setCharacterEncoding("euc-kr"); %>
<!--해당 문서 내 한글 폰트 손상 관리헌다.-->
<%
	 Connection conn = null;
	 PreparedStatement pstmt = null;
	 // DB와 연결된 커녁션을 생성 후 필요시에만 임시로 빌려서 사용 후 반환하는 방식이다.
	 //DB 연결로부터 SQL을 실행이 가능하도록 하는 메소드이며 SQL문을 미리 생성 후 변수를 입력받는 방식이다.

	 String jdbc_driver = "com.mysql.jdbc.Driver";	
	 // 자바에서 DB로 접근하는 정보 변수이다.
	 String jdbc_url = "jdbc:mysql://localhost:3306/xml";
	 // 해당 DB에 대한 경로이다.
	 String jid = request.getParameter("fid");
	 // fid 파라미터값을 jid에 반환한다
	 String pw = "";
	 // pw 변수를 초기화한다. 
	try{
		Class.forName(jdbc_driver);		 // DB 드라이버를 확인한다.
		conn = DriverManager.getConnection(jdbc_url,"root","1234");
		// 해당 DB에 접근한다
		String sql = "select pw from nsfml where id = '" + jid + "'";
		// sql 구문을 작성한다. id가 jid일때 pw값을 찾는다. 
		pstmt = conn.prepareStatement(sql);
		// 연결한 DB에 해단 sql문을 입력한다.
		ResultSet rs = pstmt.executeQuery();	
		 // sql문이 실행된 값이 rs 객체로 반환된다.

		Boolean password = false; // 부울 변수 password를 생성한다. 초기값은 false이다.
		while(rs.next()) //rs.next()를 이용하여 값이 있을 때 까지 반복을 돌린다. 
		{
			pw = rs.getString("pw"); //pw에 해당 데이터베이스에있는 pw값을 반환한다.
			password = true; // password는 true로 변환한다.
		}
		
%>
<html>
   <head>
       <meta charset="EUC-KR"> <!--비밀번호를 찾기 위한 테이블을 생성한다.-->
	    <title> 비밀번호 찾기 </title>
   </head>
    <body bgcolor="#EAEAEA" >
		<h2 align="center">비밀번호 찾기</h2>
		<hr>
	    <table bgcolor= "#FFFFFF" border="1" width="280" align="center">
        <tr>
			
			<td width="150" align="left">
				<%
					if(password) // 패스워드가 true일때
					{
					out.print(jid + "의" );	
				%>	비밀번호 :
				<%
					out.print(pw + "입니다.");		
					}
					// 해당 id에대한 비밀번호를 출력한다.
					else
					{
					out.println("<script>");
					out.println("alert('아이디가 존재하지 않습니다.')");
					out.println("location.href='findpa.jsp'");
					out.println("</script>");
					}
					// 아닐 경우에는 아이디가 존재하지않는다고 스크립트를 뛰워주며 findpa으로 이동한다.
				%>
			</td>
  		</tr>
		<tr>
			<td width="150" align="center">
		<input type="button" value="로그인 화면으로" onclick="location.href='login.jsp'">
			</td>
		</tr>
		</table>
	</body>
</html>
<%
		rs.close();
		pstmt.close();
		conn.close();
		// sql문의 실행과 DB 연결 헤제한다.

	}
	catch(Exception e) {
		System.out.println(e);
	}
	
	//에러 발생시 e값(에러발생한 원인에 대한값을)출력한다.

%>
